# SpiKot

SpiKot is a Kotlin library that provides useful extensions for the Spigot API.


# Usage

To use Spikot in your project, you will need to add the following to your build.gradle.kts file:

```kotlin
repositories {
    maven("https://gitlab.com/api/v4/projects/42786260/packages/maven")
}

dependencies {
    implementation("com.gitlab.djsushi123:spikot:<latest-version>")
}
```


# Contributing

If you find a bug or want to suggest a new feature, please open an issue or a pull request on the GitLab repository.
