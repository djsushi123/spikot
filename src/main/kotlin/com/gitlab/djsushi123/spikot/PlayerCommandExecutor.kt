package com.gitlab.djsushi123.spikot

import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

public abstract class PlayerCommandExecutor : CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player) {
            sender.sendMessage("This command can only be run by players!")
            return true
        }
        return onPlayerCommand(sender, command, label, args)
    }

    /**
     * Just like [onCommand], however, it gets run only if the command is run by a player. Otherwise, a message gets
     * sent to the caller saying "This command can only be run by players!"
     */
    public abstract fun onPlayerCommand(
        sender: Player,
        command: Command,
        label: String,
        args: Array<out String>
    ): Boolean
}