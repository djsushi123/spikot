package com.gitlab.djsushi123.spikot

import org.bukkit.command.CommandException
import org.bukkit.command.PluginCommand
import org.bukkit.plugin.java.JavaPlugin

/**
 * Shorthand for `getCommand("name")!!`.
 * Can be for example used like this:
 *
 * ```kotlin
 * class Elixir : JavaPlugin() {
 *
 *     override fun onEnable() {
 *         // Enable command
 *         command("name") {
 *             setExecutor(...)
 *         }
 *     }
 * }
 * ```
 */
public fun JavaPlugin.command(name: String, builder: PluginCommand.() -> Unit = {}): PluginCommand {
    val command = getCommand(name) ?: throw CommandException("Command with name '$name' couldn't be loaded.")
    command.builder()
    return command
}